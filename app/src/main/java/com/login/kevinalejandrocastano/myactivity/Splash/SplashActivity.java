package com.login.kevinalejandrocastano.myactivity.Splash;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;

import com.login.kevinalejandrocastano.myactivity.Activities.LoginActivity;
import com.login.kevinalejandrocastano.myactivity.Activities.MainActivity;
import com.login.kevinalejandrocastano.myactivity.Utils.Util;

public class SplashActivity extends AppCompatActivity {

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);

         Intent intenLogin = new Intent(this, LoginActivity.class);
        Intent intenMain = new Intent(this, MainActivity.class);

        if (!TextUtils.isEmpty(Util.getUserMailPrefs(prefs)) &&  !TextUtils.isEmpty(Util.getUserPassPrefs(prefs))){
            startActivity(intenMain);
        } else {
            startActivity(intenLogin);
        }
        finish();
    }

}
