package com.login.kevinalejandrocastano.myactivity.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.login.kevinalejandrocastano.myactivity.R;
import com.login.kevinalejandrocastano.myactivity.Utils.Util;

public class LoginActivity extends AppCompatActivity {

    private SharedPreferences prefs;

    private EditText editextEmail;
    private EditText editextPassword;
    private Switch switchRemenber;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        bindUI();

        prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);

        setCredentialsIfExist();


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = editextEmail.getText().toString();
                String password = editextPassword.getText().toString();

               if (login(email, password)){
                   goToMain();
                   saveOnPreferences(email, password);
               }

            }
        });
    }
    private void bindUI() {

        editextEmail = (EditText) findViewById(R.id.editextEmail);
        editextPassword = (EditText) findViewById(R.id.editTextPassword);
        switchRemenber = (Switch) findViewById(R.id.switchRemember);
        btnLogin = (Button) findViewById(R.id.btnLogin);


    }

    private void  setCredentialsIfExist(){
     String email = Util.getUserMailPrefs(prefs);
     String password = Util.getUserPassPrefs(prefs);

     if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) ){
         editextEmail.setText(email);
         editextPassword.setText(password);
     }


    }



    private boolean login (String email, String password){
        if (!isValidEmail(email)){
            Toast.makeText(this, "Email is not valid, please try again", Toast.LENGTH_LONG).show();
            return false;
        } else  if (!isValidPassword(password)){
            Toast.makeText(this, "Password is not valid, 4 characters or more, please try again", Toast.LENGTH_LONG).show();
            return false;
        }else {
            return true;
        }

    }

    /* save preferences with check method */
    private void  saveOnPreferences(String email, String password){
        if (switchRemenber.isChecked()){
            SharedPreferences.Editor editor= prefs.edit();
            editor.putString("email", email);
            editor.putString("password", password);
            /*editor commit returns a boolean synchronous*/
           /* editor.commit();*/
           /* asynchronous*/
            editor.apply();
        }
    };

    /*"Patterns" api that has a library for email validation type Boolean (for more information search documentation)*/
    private boolean isValidEmail(String email){

        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValidPassword(String password){

        return password.length() >= 4;
    }

    private void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        /*action that does not go back to the login if it does not leave the application*/
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

}
